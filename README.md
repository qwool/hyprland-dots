# hyprland dots

![screenshot](./screenshot.png)

copy everything into your ~/.config/, make an issue if something isnt included or doesnt work

deps: ```waybar hyprland-git tofi wlogout kitty lux wpctl grim```

USED:

- [mnussbaum/base16-waybar](https://github.com/mnussbaum/base16-waybar) for css base16 schemes
